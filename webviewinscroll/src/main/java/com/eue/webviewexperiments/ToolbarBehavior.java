package com.eue.webviewexperiments;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.View;


/**
 * Created by kdaugavet on 18/09/15.
 */
public class ToolbarBehavior extends CoordinatorLayout.Behavior<Toolbar> {

    public ToolbarBehavior(Context context, AttributeSet attrs) {

    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, Toolbar child, View dependency) {
        return dependency instanceof Snackbar.SnackbarLayout || dependency instanceof AppBarLayout;
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, Toolbar child, View dependency) {
        if (dependency instanceof Snackbar.SnackbarLayout) {
            float translationY = Math.min(0, dependency.getTranslationY() - dependency.getHeight());
            child.setTranslationY(translationY);
        } else {
            CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) child.getLayoutParams();
            int bottomMargin = lp.bottomMargin;
            int distanceToScroll = child.getHeight() + bottomMargin;
            float ratio = dependency.getY()/(float)dependency.getHeight();
            child.setTranslationY(-distanceToScroll * ratio);
        }
        return true;
    }

    @Override
    public void onDependentViewRemoved(CoordinatorLayout parent, Toolbar child, View dependency) {
        child.setTranslationY(0);
    }
}

